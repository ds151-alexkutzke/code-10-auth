import React, { useContext, useState, useEffect } from "react";
import { Text, Input, Button } from "@rneui/base";
import { AuthContext } from "../context/AuthContext";
import * as WebBrowser from 'expo-web-browser';
import { makeRedirectUri, useAuthRequest } from 'expo-auth-session';

const discovery = {
  authorizationEndpoint: 'https://www.reddit.com/api/v1/authorize.compact',
  tokenEndpoint: 'https://www.reddit.com/api/v1/access_token',
};

const LoginScreen = ({ navigation }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { authState, signIn, tryLocalSignIn } = useContext(AuthContext);

  const [request, response, promptAsync] = useAuthRequest(
    {
      clientId: 'G-9I6OjDmqJ8qw',
      scopes: ['identity'],
      responseType: 'token',
      // For usage in managed apps using the proxy
      redirectUri: makeRedirectUri({useProxy: true}),
    },
    discovery
  );

  /*useEffect(() => {
    if (authState.signedIn) navigation.navigate("Home");
  }, [authState.signedIn]);*/

  useEffect(() => {
    if (response?.type === 'success') {
      const { code } = response.params;
      console.log(response);
      }
  }, [response]);

  useEffect(() => {
    tryLocalSignIn();
  },[]);

  return (
    <>
      <Input
        placeholder="Username"
        onChangeText={(value) => setUsername(value)}
        value={username}
      />
      <Input
        placeholder="Password"
        onChangeText={(value) => setPassword(value)}
        value={password}
        secureTextEntry={true}
      />
      <Button
        title="Entrar"
        onPress={() => {
          signIn({ username, password });
        }}
      />
      {authState.error ? <Text>{authState.error}</Text> : null}
      <Button
        disabled={!request}
        title="Login Reddit"
        onPress={() => {
          promptAsync({useProxy: true});
          }}
      />
    </>
  );
};

export default LoginScreen;
