import React, { useEffect } from "react";
import { Text } from "@rneui/base";
import gitlab from '../api/gitlab';

const HomeScreen = ({ navigation }) => {
  useEffect(() => {
    async function projects(){
      const response = await gitlab.get('/projects')
      console.log(response);
    }

    projects();

  }, []);

  return (
    <>
      <Text>HomeScreen</Text>
    </>
  );
};

export default HomeScreen;
